#GNU Makefile
TARGET   := maths
TARGET_T := $(addsuffix _testing, $(TARGET))
MAIN_CC  := prog.cc
SRCS     := $(MAIN_CC)
SRCS_T   := $(filter-out $(MAIN_CC), $(SRCS)) catch.cc tests.cc
OBJS     := $(SRCS:.cc=.o)
OBJS_T   := $(SRCS_T:.cc=.o)

ifeq ($(word 1, $(shell which colrm)), which)
BRANCH   := $(shell git branch |grep ^* |awk '{ print $2 }')
else
BRANCH   := $(shell git branch |grep ^* |colrm 1 2)
endif

ifeq ($(MAKECMDGOALS), develop)
BRANCH   := develop
endif
ifeq ($(MAKECMDGOALS), make_ci)
BRANCH   := master
endif

ifeq ($(BRANCH), master)
CXXFLAGS += -O3
else
CXXFLAGS += -Og -g
.DEFAULT_GOAL := develop
endif

$(TARGET) : $(SRCS) $(OBJS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o $(TARGET) $(OBJS)

$(TARGET_T) : $(SRCS_T) $(OBJS_T)
	$(CXX) $(CXXFLAGS) -o $(TARGET_T) $(OBJS_T)

.PHONY: develop
develop : $(TARGET_T)
	./$(TARGET_T)
	$(CXX) $(CXXFLAGS) -c $(MAIN_CC)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o $(TARGET) $(OBJS)

.PHONY: make_ci
make_ci : $(TARGET_T) $(TARGET)

prog.o : vector.hh
tests.o : vector.hh

%.o : %.c
	$(CXX) $(CXXFLAGS) -c $?

.PHONY: clean
clean :
	rm -f $(TARGET) $(TARGET_T) $(OBJS) $(OBJS_T)
