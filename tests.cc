#include "include/catch.hpp"
#include "vector.hh"

SCENARIO( "Vectors can be used in arithmetic operations.", "[vect]" ) {

	GIVEN( "Two vectors with initial values" ) {
		vect v1 = {1.0, 2.0};
		vect v2 = {3.0, 4.0};

		REQUIRE( v1.x == 1.0 );
		REQUIRE( v1.y == 2.0 );
		REQUIRE( v2.x == 3.0 );
		REQUIRE( v2.y == 4.0 );

		WHEN( "they are summed" ) {
			vect res = v1 + v2;

			THEN( "the result is a memberwise sum" ) {
				CHECK( res.x == v1.x + v2.x );
				CHECK( res.y == v1.y + v2.y );
			}
		}

		WHEN( "they are subracted" ) {
			vect res = v1 - v2;

			THEN( "the result is a memberwise subtraction" ) {
				CHECK( res.x == v1.x - v2.x );
				CHECK( res.y == v1.y - v2.y );
			}
		}

		WHEN( "their dot product is calculated" ) {
			double res = v1 * v2;

			THEN( "the result is a scalar" ) {
				CHECK( res == v1.x*v2.x + v1.y*v2.y );
			}
		}
	}

	GIVEN( "two vectors with the same members" ) {
		vect v1 = {1.0, 2.0};
		vect v2 = {1.0, 2.0};

		WHEN( "they are compared" ) {
			THEN( "they compare equal" ) {
				CHECK( v1 == v2 );
			}
		}

	}
}


