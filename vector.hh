#include <iostream>
#include <cmath>

struct vect {
	double x;
	double y;
};

vect operator- (vect v) {
	return {-v.x, -v.y};
}

vect operator+ (vect lhs, vect rhs) {
	return {lhs.x + rhs.x, lhs.y + rhs.y};
}

vect operator- (vect lhs, vect rhs) {
	return lhs + -rhs;
}

double operator* (vect lhs, vect rhs) {
	return lhs.x*rhs.x + lhs.y*rhs.y;
}

bool operator== (vect lhs, vect rhs) {
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

bool operator!= (vect lhs, vect rhs) {
	return !(lhs == rhs);
}

bool operator< (vect lhs, vect rhs) {
	return sqrt(rhs.x*rhs.x+rhs.y*rhs.y) < sqrt(rhs.x*rhs.x+rhs.y*rhs.y);
}

std::ostream& operator<< (std::ostream& o, vect v) {
	o << "{" << v.x << ", " << v.y << "}";
	return o;
}
