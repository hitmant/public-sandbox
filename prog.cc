#include <iostream>
#include "vector.hh"

int main() {
	vect v{0.0, 0.0};
	vect y{5.5, 5.5};

	std::cout << "Vector v: " << v << '\n';
	std::cout << "Vector y: " << y << '\n';
	std::cout << "Vector vy: " << v*y << '\n';

	return 0;
}
